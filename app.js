new Vue({
  el: '#app',
  data: {
    playerHealth: 100,
    monsterHealth: 100,
    gameIsRunning: false,
    turns: []
  },
  methods: {
    startGame: function() {
      this.gameIsRunning = true;
      this.playerHealth = 100;
      this.monsterHealth = 100;
      this.turns = [];
    },
    attack: function() {
      const PL_MAX_DMG = 10;
      const PL_MIN_DMG = 3;

      var damage = this.calculateDamage(PL_MIN_DMG, PL_MAX_DMG);
      this.monsterHealth -= damage;
      this.attackLog('Player', 'Monster', damage);
      this.monsterAttacks();
    },
    attackLog: function(attacker, attacked, damage, specialAttack) {
      PLAYER_STRING = 'Player';

      this.turns.unshift({
        isPlayer: attacker === PLAYER_STRING,
        text: `${attacker} hits ${attacked} ${specialAttack ? 'hard' : ''} for ${damage}`
      });
    },
    monsterAttacks: function() {
      const MT_MAX_DMG = 12;
      const MT_MIN_DMG = 5;

      var damage = this.calculateDamage(MT_MIN_DMG, MT_MAX_DMG);
      this.playerHealth -= damage;
      this.attackLog('Monster', 'Player', damage);
      this.checkWin();
    },
    specialAttack: function() {
      const PL_MAX_DMG = 10;
      const PL_MIN_DMG = 20;

      var damage = this.calculateDamage(PL_MIN_DMG, PL_MAX_DMG);
      this.monsterHealth -= damage;
      this.attackLog('Player', 'Monster', damage, true);
      this.monsterAttacks();
    },
    calculateDamage: function(min, max) {
      return max && min ? Math.max(Math.floor(Math.random() * max) + 1, min) : 0
    },
    checkWin: function() {
      if (this.playerHealth <= 0) {
        this.playerHealth = 0;
        this.gameIsRunning = false;
        if (confirm('You lost! New Game?')) {
          this.startGame();
        }
        return;
      } else if (this.monsterHealth <= 0) {
        this.monsterHealth = 0;
        this.gameIsRunning = false;
        alert('You won!');
        return;
      }
    },
    heal: function() {
      if (this.playerHealth <= 90) this.playerHealth += 10;
      this.monsterAttacks();
    },
    giveUp: function() {
      this.gameIsRunning = false;
    }
  }
});